# -*- coding: utf-8 -*-
# Copyright (c) 2021, k and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.integrations.utils import create_request_log, create_payment_gateway


class PayconvergeSettings(Document):
    supported_currencies = ["INR","CAD"]

    def validate(self):
        create_payment_gateway('Payconverge')

    def validate_transaction_currency(self, currency):
        if currency not in self.supported_currencies:
            frappe.throw(_("Please select another payment method. converge does not support transactions in currency '{0}'").format(currency))

    def get_payment_url(self, **kwargs):
        pass

