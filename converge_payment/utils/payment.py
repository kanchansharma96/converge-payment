
from __future__ import unicode_literals, print_function
import frappe
import json
import requests

VALID_TRANSACTION_TYPES = [
    'ccauthonly',
    'ccavsonly',
    'ccsale',
    'ccverify',
    'ccgettoken',
    'cccredit',
    'ccforce',
    'ccbalinquiry',
    'ccgettoken',
    'ccreturn',
    'ccvoid',
    'cccomplete',
    'ccdelete',
    'ccupdatetip',
    'ccsignature',
    'ccaddrecurring',
    'ccaddinstall',
    'ccupdatetoken',
    'ccdeletetoken',
    'ccquerytoken'
]


class ConvergenceException(Exception):
    pass


class Converge(object):
    def __init__(self, merchant_id, user_id, pin, is_demo=False):
        self._merchant_id = merchant_id
        self._user_id = user_id
        self._pin = pin
        self._is_demo = is_demo

    @property
    def xml_endpoint(self):
        if self._is_demo:
            return "https://api.demo.convergepay.com/VirtualMerchantDemo/process.do"
        return "https://api.convergepay.com/VirtualMerchant/process.do"

    def request(self, transaction_type, **kwargs):
        """
        Make request to Converge with transaction type and parameters.
        :param transaction_type: a valid transaction type that's in VALID_TRANSACTION_TYPES
        :param kwargs:
        :return:
        """
        if transaction_type not in VALID_TRANSACTION_TYPES:
            raise ConvergenceException(
                '{} is not a valid transaction type'.format(transaction_type))

        kwargs['ssl_transaction_type'] = transaction_type
        return self._http_request(**kwargs)

    def _http_request(self, **kwargs):
        kwargs['ssl_merchant_id'] = self._merchant_id
        kwargs['ssl_user_id'] = self._user_id
        kwargs['ssl_pin'] = self._pin
        kwargs['ssl_show_form'] = 'false'
        kwargs['ssl_result_format'] = 'ascii'

        response = requests.post(self.xml_endpoint, kwargs)
        print(type(response))
        response.raise_for_status()
        result = {}
        decoded = response.content.decode('utf-8')
        print(type(decoded))
        for line in decoded.split('\n'):
            k, v = line.split('=')
            result[k] = v
        return result


@frappe.whitelist(allow_guest=True)
def converge_payment(cardNumber=None, expiryMonth=None,  cvvCode=None,amount=None,salesid=None):
    merchant_id = frappe.get_doc("Payconverge Settings").merchant_id
    user_id = frappe.get_doc("Payconverge Settings").user_id
    pin = frappe.get_doc("Payconverge Settings").pin
   
    convergepayval = Converge(merchant_id,user_id,pin, is_demo=True)
    details = convergepayval.request('ccsale',  # ccsale,ccverify
        ssl_card_number=cardNumber,
        ssl_exp_date=str(expiryMonth),
        ssl_cvv2cvc2=cvvCode,
        ssl_amount=amount)
    print(details)
    try:
        if details['ssl_result_message']=='APPROVAL':
            payment_entry_name=frappe.db.sql("""select name from `tabPayment Request` where reference_name="{}" """.format(salesid),as_dict=1)
            print(payment_entry_name)
            payment_id=payment_entry_name[0].name
            sales_invoice_data=frappe.db.sql("""select * from `tabSales Invoice` where name="{}" """.format(salesid),as_dict=1)
            abbrevation=frappe.db.sql("""select abbr from `tabCompany` where name="{}" """.format(sales_invoice_data[0].company),as_dict=1);
            user = frappe.get_doc({
                "doctype": "Payment Entry",    
                "payment_type":'Receive',             
                "company": sales_invoice_data[0].company,   #salesinvoice.company
                "party_type":'Customer',
                "mode_of_payment":'Credit Card',
                "party":sales_invoice_data[0].customer,    #salesinvoice.customer
                "paid_amount":float(sales_invoice_data[0].grand_total),      #salesinvoice.grand_total
                "received_amount":float(sales_invoice_data[0].grand_total),   #salesinvoice.grand_total
                "reference_no":payment_id,    #paymentrequest.name
                "paid_to":"Cash - "+abbrevation[0].abbr, #salesinvoice.company.abbrevation
                "references":[{
                    "reference_doctype":"Sales Invoice",
                    "reference_name":sales_invoice_data[0].name  ,   #salesinvoice.name
                    "allocated_amount":float(sales_invoice_data[0].grand_total)  #salesinvoice.grand_total
                    }]
                })
            user.save()
            user.submit()
            return "APPROVAL"
    except:
        if details['errorName']=="Credit Card Number Invalid":
            return "card"
        elif details['errorName']=="Exp Date Invalid":
            return "expiry"
        elif details['errorName']=="Invalid CVV2 Value":
            return "cvv"
        else:
            pass














