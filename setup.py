# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in converge_payment/__init__.py
from converge_payment import __version__ as version

setup(
	name='converge_payment',
	version=version,
	description='converge payment',
	author='k',
	author_email='k@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
